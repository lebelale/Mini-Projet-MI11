/*----------------------------------------------------------------------------*
 * fichier : noyau_test.c                                                     *
 * programme de test du noyaut                                                *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdlib.h>

#include "stm32h7xx.h"
#include "serialio.h"
#include "noyau_prio.h"
#include "noyau_file_prio.h"
#include "delay.h"
#include "TERMINAL.h"

typedef struct{
	char* name;
	uint16_t id;
	uint32_t duration;
} AP_TASK_PARAMS;

TACHE init(void);
TACHE tacheGen(void);
TACHE bandeauLed(void);
TACHE tachedefond(void);
TACHE randomTacheAP(AP_TASK_PARAMS *params);

int main();

uint32_t duration1 = 50;
uint32_t duration2 = 150;
uint32_t duration3 = 100;

#define HIGH_PRIO_LONG_ENOUGH_TIME 1000
#define HIGH_PRIO_MEDIUM_TIME 500
#define HIGH_PRIO_CPU_TIME_SCARCE 100

#define MAX_CARA_LIGNE 80

/*
 * structure du contexte d'une tache
 */
typedef struct
{
	// adresse de debut de la tache
	uint16_t Nb_tour;
	// etat courant de la tache
	uint16_t wait_time;
} NOYAU_TCB_ADD;

/*----------------------------------------------------------------------------*
 * declaration des variables du noyau comme extern pour pouvoir les
 * utiliser dans d'autres partie du code
 * *--------------------------------------------------------------------------*/
#define POS_CHRONO 10
/*
 * tableau stockant le contexte de chaque tache
 */
NOYAU_TCB_ADD _noyau_tcb_add[MAX_TACHES_NOYAU];

uint16_t pos_x = 1;
uint16_t pos_y = 10;

void init(void)
{
	// Lancement des tâches périodiques
	start((TACHE_ADR)bandeauLed);

	// fin_tache();
}

TACHE bandeauLed(void)
{
	uint16_t id;
	SET_CURSOR_POSITION(3, 1);
	puts("------> EXEC tache de fond");

	id = 3; // Priorité 0 et id 3
	_noyau_tcb_add[id].Nb_tour = 1;
	/*
	 * WAIT TIME :
	 * 1000 : HIGH_PRIO_LONG_ENOUGH_TIME
	 * 500 : MEDIUM_PRIO_MEDIUM_TIME
	 * 100 : HIGH_PRIO_CPU_TIME_SCARCE
	*/
	_noyau_tcb_add[id].wait_time = HIGH_PRIO_MEDIUM_TIME;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 8;
	_noyau_tcb_add[id].Nb_tour = 2;
	_noyau_tcb_add[id].wait_time = 400;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 16;
	_noyau_tcb_add[id].Nb_tour = 4;
	_noyau_tcb_add[id].wait_time = 60;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 18;
	_noyau_tcb_add[id].Nb_tour = 4;
	_noyau_tcb_add[id].wait_time = 40;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 24;
	_noyau_tcb_add[id].Nb_tour = 3;
	_noyau_tcb_add[id].wait_time = 15;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 31;
	_noyau_tcb_add[id].Nb_tour = 3;
	_noyau_tcb_add[id].wait_time = 15;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 32;
	_noyau_tcb_add[id].Nb_tour = 2;
	_noyau_tcb_add[id].wait_time = 10;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 37;
	_noyau_tcb_add[id].Nb_tour = 3;
	_noyau_tcb_add[id].wait_time = 10;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 40;
	_noyau_tcb_add[id].Nb_tour = 3;
	_noyau_tcb_add[id].wait_time = 5;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 48;
	_noyau_tcb_add[id].Nb_tour = 1;
	_noyau_tcb_add[id].wait_time = 50;
	active(cree((TACHE_ADR)tacheGen, id, (void *)&_noyau_tcb_add[id]));
	id = 0b111011;
	_noyau_tcb_add[id].Nb_tour = 50;
	_noyau_tcb_add[id].wait_time = 50;
	active(cree((TACHE_ADR)tachedefond, id, (void *)&_noyau_tcb_add[id]));
	while (1){};
}

TACHE tachedefond()
{
	extern uint16_t _tache_c;
	uint16_t id_tache_fond = _tache_c;

	while (1)
	{
		for (int i = 0; i < ap_task_count; i++)
		{
			if (ap_task_list[i].active)
			{
				(ap_task_list[i].handler)(ap_task_list[i].params);
			}
		}

		printf("\ntache de fond ! ap_task_count : %d\n", ap_task_count);
		// delay_n_ticks(20);

		tache_reset_flag_tick(id_tache_fond);
	}
	return;
}

void randomTacheAP(AP_TASK_PARAMS *params)
{

	// Timing
	// Start tick
	uint32_t duration = params->duration;
	printf("\nhello from task, name : %s, duration : %d ticks\n", params->name, duration);

	delay_n_ticks(duration);
	printf("\ngoodbye from randomTacheAP");
	ap_task_list[params->id].active = 0;
	ap_task_count--;

	return;
}


TACHE tacheGen(void)
{
	NOYAU_TCB *p_tcb = NULL;
	uint16_t id_tache;
	uint16_t i, j = 1;

	id_tache = noyau_get_tc();
	p_tcb = noyau_get_p_tcb(id_tache);
	id_tache = p_tcb->id;

	uint16_t Nb_tour = ((NOYAU_TCB_ADD *)(p_tcb->tcb_add))->Nb_tour;
	uint16_t wait_time = ((NOYAU_TCB_ADD *)(p_tcb->tcb_add))->wait_time;

	// on laisse du temps à la tâche de fond de démarrer toutes les tâches
	delay_n_ticks(20);
	while (1)
	{
		id_tache = noyau_get_tc();
		while (tache_get_flag_tick(id_tache) != 0)
		{
			_lock_();
			printf("%u ", p_tcb->id);
			// printf("%u \n", _noyau_tcb_add[id_tache].wait_time);
			// for (i = POS_CHRONO; i < (POS_CHRONO + 8); i++)
			// {
			// 	printf("%s%d;%d%s", CODE_ESCAPE_BASE, i, pos_x, "H");
			// 	if ((i - POS_CHRONO) == (id_tache >> 3))
			// 	{
			// 		SET_CURSOR_POSITION(i, pos_x);
			// 		SET_BACKGROUND_COLOR(id_tache + 16);
			// 		SET_FONT_COLOR(15);
			// 		printf("%2d", id_tache);
			// 		SET_BACKGROUND_COLOR(0);
			// 	}
			// 	else
			// 	{
			// 		SET_BACKGROUND_COLOR(0);
			// 		printf("  ");
			// 	}
			// }
			// pos_x = pos_x + 2;
			// if (pos_x > MAX_CARA_LIGNE)
			// {
			// 	pos_x = 1;
			// }

			if (ap_task_count < MAX_AP_TASKS &&  p_tcb->id == 3)
			{
				creerTacheAperiodique(randomTacheAP, 60, "ESP32", duration1);
				creerTacheAperiodique(randomTacheAP, 61, "ABCDEF", duration3);
			}
			if (ap_task_count < MAX_AP_TASKS &&  p_tcb->id == 8)
			{
				creerTacheAperiodique(randomTacheAP, 62, "STM32H7", duration2);

			}
			if (j >= Nb_tour)
			{
				j = 1;
				delay_n_ticks(wait_time);
			}
			else
			{
				j++;
			}
			_unlock_();
			tache_reset_flag_tick(id_tache);
		}
	}
}

int main()
{
	usart_init(115200);
	CLEAR_SCREEN(1);
	SET_CURSOR_POSITION(1, 1);
	test_colors();
	CLEAR_SCREEN(1);
	SET_CURSOR_POSITION(1, 1);
	puts("Test noyau");
	puts("Noyau preemptif");
	SET_CURSOR_POSITION(5, 1);
	SAVE_CURSOR_POSITION();
	init();

	return (0);
}
